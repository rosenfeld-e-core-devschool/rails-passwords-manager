class CreatePasswords < ActiveRecord::Migration[7.0]
  def change
    create_table :passwords do |t|
      t.references :user, null: false, foreign_key: true
      t.string :title
      t.string :username
      t.string :password
      t.string :comments

      t.timestamps
    end
  end
end
