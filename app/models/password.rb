class Password < ApplicationRecord
  belongs_to :user
  validates :title, presence: true

  def self.filter(user, title)
    where(user: user).where('lower(title) like ?', "%#{sanitize_sql_like(title.downcase)}%")
  end
end
