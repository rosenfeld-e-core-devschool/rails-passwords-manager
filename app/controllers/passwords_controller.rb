class PasswordsController < ApplicationController
  before_action :set_password, only: %i[ show edit update destroy ]

  # GET /passwords or /passwords.json
  def index
    list_passwords Password.where(user: current_user)
  end

  private def list_passwords(passwords)
    @passwords = passwords
    render 'index'
  end

  # GET /passwords/1 or /passwords/1.json
  def show
    #respond_to do |format|
      #format.html
      #format.turbo_stream
    #end
  end

  # GET /passwords/new
  def new
    @password = Password.new
    respond_to do |format|
      format.html
      format.turbo_stream
    end
  end

  # GET /passwords/1/edit
  def edit
    respond_to do |format|
      format.html
      format.turbo_stream
    end
  end

  # POST /passwords or /passwords.json
  def create
    @password = Password.new(password_params)
    @password.user = current_user

    respond_to do |format|
      if @password.save
        format.html { redirect_to password_url(@password), notice: "Password was successfully created." }
        format.json { render :show, status: :created, location: @password }
        format.turbo_stream
      else
        # TODO: handle turbo_stream format
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @password.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /passwords/1 or /passwords/1.json
  def update
    respond_to do |format|
      if @password.update(password_params)
        format.html { redirect_to password_url(@password), notice: "Password was successfully updated." }
        format.json { render :show, status: :ok, location: @password }
        format.turbo_stream { render :show }
      else
        # TODO: handle turbo_stream response when update fails
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @password.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /passwords/1 or /passwords/1.json
  def destroy
    @password.destroy

    respond_to do |format|
      format.html { redirect_to passwords_url, notice: "Password was successfully destroyed." }
      format.json { head :no_content }
      format.turbo_stream
    end
  end

  def filter
    title = params[:title_filter].downcase
    passwords = Password.filter current_user, title
    list_passwords passwords
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_password
      @password = Password.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def password_params
      params.require(:password).permit(:user_id, :title, :username, :password, :comments)
    end
end
