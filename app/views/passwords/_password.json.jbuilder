json.extract! password, :id, :user_id, :title, :username, :password, :comments, :created_at, :updated_at
json.url password_url(password, format: :json)
