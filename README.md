# README

Instructions:

* Install Ruby

Run:

    bundle
    bin/rails db:migrate
    bin/rails server -p 3000

Run tests with

    bin/rails test
